import pandas as pd
import os
import pandas_datareader as pdr

from datetime import datetime
from helpers.connector import AZURESQlDatabaseServer
from helpers.slack import post_message

tiingo_api_key = os.getenv('TIINGO_API_KEY')

if __name__=='__main__':
	try:

		server = AZURESQlDatabaseServer()

		stock_data = pdr.get_data_tiingo('GOOG', api_key=tiingo_api_key)
		stock_data.reset_index(inplace=True)
		stock_data.date = stock_data.date.dt.date
 
		# ETL Process
		server.drop('staging.staging_temp_market_data')
		server.load(stock_data, 'staging.staging_temp_market_data', "replace")
		server.update('SQL/ETL/StockData/truncate_company.sql')
		server.update('SQL/ETL/StockData/update_company.sql')
		server.drop('staging.staging_temp_market_data')

		post_message(message='Hooray!! company.py has run successfully:)', channel='cronjobautomation')
	except Exception as e:
		post_message(message=f'The script company.py has failed:(.{e}', channel='cronjobautomation')
		raise
