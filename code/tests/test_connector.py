import pytest

# import os
# print(f'The current directory is --- {os.getcwd()}')

from ..helpers.connector import AZURESQlDatabaseServer

server = AZURESQlDatabaseServer()

class TestDatabaseConnection:
	def test_get_environment_variables(self):
		assert server.db != None
		assert server.server != None
		assert server.pw != None
		assert server.user != None