import requests
import json

def post_message(message, channel):
	webhooks_channel_dict = {
		'cronjobautomation': 'https://hooks.slack.com/services/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'
	}

	data = {
		'text':message,
		'username': 'Wall-E',
        'icon_emoji': ':robot_face:'
	}

	response = requests.post(webhooks_channel_dict[channel], data=json.dumps(data), headers={'Content-Type': 'application/json'})

	return None