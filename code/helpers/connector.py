import os
import pandas as pd
import pymssql
import pyodbc
from sqlalchemy import create_engine

import logging


class Connect(object):

	def __init__(self):
		self.db = None
		self.server = None
		self.pw = None
		self.user = None
		self.conn = None
		self.sql = None
		self.headers = None

	def __str__(self):
		return f'Connection name: {self.name}'

class AZURESQlDatabaseServer(Connect):
	
	def __init__(self):
		
		Connect.__init__(self)
		self.db = os.environ.get('AZSQL_DB')
		self.server = os.environ.get('AZSQL_HOST')
		self.pw = os.environ.get('AZSQL_PASSWORD')
		self.user = os.environ.get('AZSQL_USER')
		#self.sql = os.environ.get('WHARTEVER')

	def connect(self):
		try:
			if self.db is None or self.db == "":
				raise OSError
			if self.server is None or self.server == "":
				raise OSError
			if self.user is None or self.user == "":
				raise OSError
			if self.pw is None or self.pw == "":
				raise OSError

			try:
				self.conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+self.server+';DATABASE='+self.db+';UID='+self.user+';PWD='+self.pw)
				logging.info(f'Successfully connected to {self.db}.')
				logging.info("connected with pyodbc")
			
				return None

			except Exception as e:
				logging.error(f'Connection error with pyodbc: {e}')
				try:
					self.conn = pymssql.connect(self.server,self.user,self.pw,self.db)             
					logging.info(f'Successfully connected to {self.db}.')
					logging.info("connected with pymssql")
					return None

				except Exception as e:
					logging.error(f'Connection error with pymssql: {e}')
					raise
		except OSError as e:
			logging.error(f"Check if AZSQL_DB, AZSQL_HOST, AZSQL_USER and AZSQL_PASSWORD environment variables are all set.")
			raise

	def open_query(self, file_name):
		try:
			file = os.path.realpath(file_name)
			with open(file, 'r') as sqlFile:
				self.sql = sqlFile.read()
				
			return self.sql
		except Exception as e:
			logging.error(f"Filename: {file_name}. File stream error: {e}")
			raise

	def execute(self, temp):
		if temp == False:
			self.connect()
		else:
			logging.info('Already Connected.')
			
		try:
			with self.conn.cursor() as curr:
				curr.execute(self.sql)
				self.headers = [desc[0] for desc in curr.description]
				fet = curr.fetchall()
				if fet:
					res = pd.DataFrame.from_records(fet)
					res.columns = self.headers
					logging.info('Successfully fetched results.')
					return res
				else:
					logging.info('No results returned from query.')
					return None

		except Exception as e:
			logging.error(f'Query error: {e}')
			raise
		finally:
			curr.close()
			self.conn.close()
			logging.info(f'Closed connection to {self.db}.')

	def query(self, file_name, temp = False, *param):
		# This function does not validate order of the format
		# Please ensure param orders are correct
		# Else the result will not be correct
		self.open_query(file_name=file_name)
		try:
			self.sql = self.sql.format(*param)

		except Exception as e:
			logging.error(f'Query format error: {e}')
			raise
		
		res = self.execute(temp)

		return res

	def create_temp(self, file_name, *param):
		self.open_query(file_name=file_name)

		try:
			self.sql = self.sql.format(*param)

		except Exception as e:
			logging.error(f'Query format error: {e}')
			raise
			
		try:
			with self.conn.cursor() as curr:
				curr.execute(self.sql)
				self.conn.commit()
				logging.info('No results returned from query.')
				return None

		except Exception as e:
			logging.error(f'Query error: {e}')
			raise

	def disconnect(self):
		try:
			self.conn.close()
			logging.info(f'Closed connection to {self.db}.')

			return None

		except Exception as e:
			logging.error(f'Connection error: {e}')
			raise


	def load(self, dataframe, table_name, if_exists):
		self.disposition = 'load'
		if if_exists is not None:
			self.write_disposition = if_exists

		schema = table_name.split('.')[0] if len(table_name.split('.')) > 1 else None
		# Create connection string and sqlalchemy engine
		self.engine = create_engine(f'mssql+pyodbc://{self.user}:{self.pw}@{self.server}:1433/{self.db}?driver=ODBC+Driver+17+for+SQL+Server')

		if schema is not None:
			dataframe.to_sql(
				schema=schema,
				name=table_name.split('.')[1],
				con=self.engine,
				if_exists=self.write_disposition,
				index=False,
				chunksize=1000)
		else:
			dataframe.to_sql(
				name=table_name,
				con=self.engine,
				if_exists=self.write_disposition,
				index=False,
				chunksize=1000)

		return None

	def drop(self, table_name):
		self.sql = f'drop table if exists {table_name}' 
		self.connect()
		try:
			with self.conn.cursor() as curr:
				curr.execute(self.sql)
				
			self.conn.commit()
			
		except Exception as e:
			logging.error(f'Query error: {e}')
			raise
			
		finally:
			curr.close()
			self.conn.close()
			logging.info(f'Closed connection to {self.db}.')
		
		return None
	
	def update(self, file_name, *param):
		self.open_query(file_name=file_name)

		try:
			if param:
				self.sql = self.sql.format(*param)

		except Exception as e:
			logging.error(f'Query format error: {e}')
			raise

		self.connect()

		try:
			with self.conn.cursor() as curr:
				curr.execute(self.sql)

			self.conn.commit()
			
		except Exception as e:
			logging.error(f'Query error: {e}')
			raise
			
		finally:
			curr.close()
			self.conn.close()
			logging.info(f'Closed connection to {self.db}.')

		return None
