CREATE TABLE dbo.MarketDataTemp (
	[date] date NULL,
	symbol varchar(255) NULL,
	adjClose float NULL,
	adjHigh float NULL,
	adjLow float NULL,
	adjOpen float NULL,
	adjVolume int NULL,
	[close] float NULL,
	divCash float NULL,
	high float NULL,
	low float NULL,
	[open] float NULL,
	splitFactor float NULL,
	volume int NULL
);
