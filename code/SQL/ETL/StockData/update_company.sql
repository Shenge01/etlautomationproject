insert into dbo.MarketDataTemp
select 
	cast("date" as date) as "date", 
	symbol, 
	adjClose, 
	adjHigh, 
	adjLow, 
	adjOpen,
	adjVolume, 
	"close", 
	divCash, 
	high, 
	low, 
	"open", 
	splitFactor,
	volume
from staging.staging_temp_market_data;