import time 
import pyodbc

from helpers.connector import AZURESQlDatabaseServer
from helpers.slack import post_message

server = AZURESQlDatabaseServer()

try:
	server.connect()
	post_message(message='Connection Sucessful...', channel='cronjobautomation')

# Adding time delay after an Operational error(database is down)
except pyodbc.OperationalError as e:
	time.sleep(30)
	server.connect()
	post_message(message='Hooray!! Connection is Sucessful...After ping', channel='cronjobautomation')
	